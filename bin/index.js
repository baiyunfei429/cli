#! /usr/bin/env node
const path = require("path");
const chalk = require("chalk");
const ora = require('ora')
const execa = require('execa')
const childProcess = require("child_process");

// console.log("hello world");
// // 打印红色hello
// const h1 = chalk.red('hello');
// console.log(h1)
// // 打印红色hello，绿色背景hello
// const h2 = chalk.red.bgGreen('hello');
// console.log(h2)
// // 自定义rgb颜色
// const h3 = chalk.rgb(155,66,211).bgRgb(36,88,144)('hello');
// console.log(h3)
// // 自定义十六进制颜色
// const h4 = chalk.hex('#ccc').bgHex('#051106')('hello');
// console.log(h4)
// // 增加文字效果 加粗，下划线，斜体 等
// const h5 = chalk.bold.underline('hello');
// console.log(h5);

// const spinner = ora('下载中，请等待')
// spinner.start()
// setTimeout(()=>{
//   spinner.stop()
// },5000);

// (async ()=>{
//   const { stdout } = await execa('echo',['hello moto']);
//   console.log(stdout);
// })()

// const commander = require("commander");
// // 定义指令 -a，可使用 --help查看到帮助信息
// commander.option("-a", "this is test -a", ()=>{
//     console.log('a console-1')
// })
// // 定义指令 -am 带参数 <num> 尖括号参数必填   [num] 中括号参数可选
// commander.option("--am <num>", "this is test -a", (num)=>{
//     console.log('a console-2', num)
// })
// // 定义命令 mycli create xxx
// commander.command('create <projectName>').action((projectName)=>{
//     console.log('a console-3', projectName)
// })
// // 定义版本
// commander.version('1.2.5')
// // 绑定解析
// commander.parse(process.argv)

// const inquirer = require('inquirer')
// // prompt 方法，传入对象数组，对象中type问题类型
// inquirer.prompt([
//     {
//         type:"input",
//         name:"username",
//         message:"请输入用户名"
//     },
//     {
//         type:"password",
//         name:"password",
//         message:"请输入密码"
//     },
//     {
//         type:"list",
//         name:"adult",
//         message:"爱好",
//         choices:['1','2','3']
//     }
// ]).then(answer=>{
//     console.log(answer)
// })

// const downgit = require('download-git-repo')
// // 指定位置 github:作者/项目，也可是gitlab
// // 下载目录 process.cwd() 是当前实际运行目录，__dirname 是始终是脚手架目录
// // 默认 {clone:false}，默认是http下载方式（推荐），如设置true是使用git clone方式
// // 回调
// const spinner = ora('下载中').start()
// downgit('github:vuejs/vue', path.join(process.cwd(), '/downgit'), {clone:false}, function(err){
//     spinner.stop()
//     if(err){
//         throw err
//     }
//     console.log(chalk.green('下载成功'));
//     (async ()=>{
//       const { stdout } = await execa('echo',['hello moto--']);
//       console.log(stdout);
//     })()
// })
childProcess.exec('cd downgit && git status', (error, stdout, stderr) => {
// childProcess.exec('cd downgit && cnpm i && npm run dev', (error, stdout, stderr) => {
    if (!error) {
        console.log(stdout)
        console.log('success')
        // 成功
    } else {
        console.log('failed')
        // 失败
    }
});